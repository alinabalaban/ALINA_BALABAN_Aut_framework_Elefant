package com.elefant.automation.pagesDefinition;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

public class ContactPage {

    @FindBy(how = How.CLASS_NAME, using = "checkout-product-title")
    private static WebElement ProductNameThird;

    @FindBy(how = How.XPATH, using = "html/body/div[5]/div[2]/div/div[2]/div[2]/div/span[2]/span")
    private static WebElement CUI;

    @FindBy(how = How.XPATH, using = "html/body/div[5]/div[2]/div/div[2]/div[2]/p[5]/span")
    private static WebElement FAX;

    @FindBy(how = How.XPATH, using = "html/body/div[5]/div[2]/div/div[2]/div[2]/p[12]/a")
    private static WebElement Email;

    public static void CheckContactPage(){
        Assert.assertEquals( CUI.getText(), "Cod Unic de Inregistrare: RO 26396066");
        Assert.assertEquals(FAX.getText(), "031.620.74.19");
        Assert.assertEquals(Email.getText(), "contact@elefant.ro");
    }



}
