package com.elefant.automation.pagesDefinition;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

import static com.elefant.automation.utils.BaseTest.TakeAPic;

public class LoginFacebookPage {

    @FindBy(how = How.XPATH, using = ".//*[@id='header_block']/span")
    private static WebElement TitleFacebook;

    @FindBy(how = How.XPATH, using = ".//*[@id='email']")
    private static WebElement Email;

    @FindBy(how = How.XPATH, using = ".//*[@id='pass']")
    private static WebElement Password;

    @FindBy(how = How.XPATH, using = ".//*[@id='loginbutton']")
    private static WebElement LoginButton;
    /**
     * Completes all details for login with facebook, and take pics of all the steps
     */

    public void AddDetailsFacebookLogin(){
//        Assert.assertEquals(TitleFacebook.getText(), "Conectează-te la Facebook");
        Assert.assertTrue(Email.isDisplayed());
        Email.click();
        Email.clear();
        Email.sendKeys("alina.irina.101@gmail.com");
        TakeAPic();
        Assert.assertEquals(Email.getAttribute("value"), "alina.irina.101@gmail.com");
        Password.click();
        Password.clear();
        Password.sendKeys("perSention123");
        Assert.assertEquals(Password.getAttribute("value"), "perSention123");
        TakeAPic();
        LoginButton.click();
        TakeAPic();
    }
}
