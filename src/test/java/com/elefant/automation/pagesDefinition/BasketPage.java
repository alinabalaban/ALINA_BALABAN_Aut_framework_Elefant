package com.elefant.automation.pagesDefinition;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

import static com.elefant.automation.utils.BaseTest.TakeAPic;
import static com.elefant.automation.utils.BaseTest.pressBack;
import static com.elefant.automation.utils.TestUtils.mySleeper;

public class BasketPage {
    public String ThirdProductName;

    @FindBy(how = How.CLASS_NAME, using = "checkout-product-title")
    private static WebElement ProductNameThird;

    @FindBy(how = How.XPATH, using = ".//*[@id='main_comanda']/div[2]/div/div/div[2]/div[3]/a[1]")
    private static WebElement MoveToWishlist;

    @FindBy(how = How.LINK_TEXT, using = "Sterge")
    private static WebElement DeleteFromBasket;

    @FindBy(how = How.XPATH, using = ".//*[@id='more_318391_1']/a")
    private static WebElement ModifyQuantity;

    /**
     * Saves The name from this page in a variable named ThirdProductName
     */
    public void getThirdName(){
        ThirdProductName=ProductNameThird.getText();
    }
    /**
     * Removes products from basket
     */
    public void removeFromBasket(){
        mySleeper(2000);
        Assert.assertTrue(DeleteFromBasket.isDisplayed());
        TakeAPic();
        DeleteFromBasket.click();
        mySleeper(2000);
        TakeAPic();
        pressBack();

    }




}
