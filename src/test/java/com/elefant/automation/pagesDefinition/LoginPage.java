package com.elefant.automation.pagesDefinition;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

import static com.elefant.automation.utils.BaseTest.TakeAPic;
import static com.elefant.automation.utils.TestUtils.mySleeper;

public class LoginPage {
    @FindBy(how = How.CLASS_NAME, using = "login-facebook-button")
    private static WebElement LoginFacebook;

    @FindBy(how = How.CLASS_NAME, using = "login-google-button")
    private static WebElement LoginGoogle;

    @FindBy(how = How.CLASS_NAME, using = "login-yahoo-button")
    private static WebElement LoginYahoo;

    @FindBy(how = How.XPATH, using = ".//*[@id='login_username']")
    private static WebElement UserName;

    @FindBy(how = How.XPATH, using = ".//*[@id='login_password']")
    private static WebElement Password;

    @FindBy(how = How.XPATH, using = ".//*[@id='login_classic']")
    private static WebElement LoginButton;

    @FindBy(how = How.XPATH, using = ".//*[@id='loginForm']/div[6]/a")
    private static WebElement CreateAccount;

    /**
     * Check if all login methods exists
     * Choose the login method from list
     * Then take a pic
     */


    public void loginMethod(int meth){
        Assert.assertTrue(LoginFacebook.isDisplayed());
        Assert.assertTrue(LoginGoogle.isDisplayed());
        Assert.assertTrue(LoginYahoo.isDisplayed());
        TakeAPic();
        switch(meth){
            case 1:  LoginFacebook.click();
                break;
            case 2:  LoginGoogle.click();
                break;
            case 3:  LoginYahoo.click();
                break;
        }
        TakeAPic();
    }

    /**
     * Classic login, with all details
     */
    public void loginMethod(){
        mySleeper(1000);
        Assert.assertTrue(UserName.isDisplayed());
        Assert.assertTrue(Password.isDisplayed());
        TakeAPic();
        UserName.click();
        UserName.clear();
        UserName.sendKeys("alina.irina.101@gmail.com");
        Assert.assertEquals(UserName.getAttribute("value"),"alina.irina.101@gmail.com" );
        TakeAPic();
        Password.click();
        Password.clear();
        Password.sendKeys("333333");
        Assert.assertEquals(Password.getAttribute("value"),"333333" );
        Assert.assertTrue(LoginButton.isDisplayed());
        TakeAPic();
        LoginButton.click();
    }
    /**
     * Clicks on Create account button
     */
    public void CreateAccount(){
        Assert.assertEquals(CreateAccount.getText(), "Creeaza cont");
        TakeAPic();
        CreateAccount.click();
        mySleeper(1000);
    }


}
