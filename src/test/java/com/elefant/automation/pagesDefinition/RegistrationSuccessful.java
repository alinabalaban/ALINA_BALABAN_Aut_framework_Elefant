package com.elefant.automation.pagesDefinition;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

import static com.elefant.automation.utils.TestUtils.mySleeper;

public class RegistrationSuccessful {

    @FindBy(how = How.XPATH, using = "html/body/div[4]/div[1]/div/div/div/div[2]/div/h3")
    public static WebElement Confirmation;

    @FindBy(how = How.XPATH, using = ".//*[@id='goto_redirect']")
    public static WebElement GoBack;
    /**
     * Confirms that the registration went well
     * Press back
     */

    public void readyAndBack(){
        mySleeper(3000);
        System.out.println(Confirmation.getText());
        //System.out.println(Confirmation.getAttribute("value"));
        Assert.assertEquals(Confirmation.getText(), "Inregistrare reusita");
        Assert.assertTrue(GoBack.isDisplayed());
        GoBack.click();

    }


}
