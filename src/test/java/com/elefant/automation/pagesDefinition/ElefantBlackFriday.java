package com.elefant.automation.pagesDefinition;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import static com.elefant.automation.utils.BaseTest.TakeAPic;
import static com.elefant.automation.utils.TestUtils.mySleeper;

public class ElefantBlackFriday {
    @FindBy(how = How.CLASS_NAME, using = "back-to-site")
    private static WebElement BackToSite;
    /**
     * Press back on the promotion if exists, and take a pic
     * If not is just taking a pic
     */

  public void BackToSite(){
      mySleeper();

      try {
          BackToSite.click();
          TakeAPic();
      }
      catch (Exception E){
          TakeAPic();

      }
      mySleeper(1000);
  }
}
