package com.elefant.automation.pagesDefinition;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

import java.util.List;

public class AuthorsPage {
    @FindBy(how = How.XPATH, using = "//SPAN[@class='title'][text()='Autori']")
    private static WebElement TitleAuthPage;
    @FindAll({@FindBy(how = How.CLASS_NAME, using = "link_litera")})
    private static List<WebElement> AuthorsLetters;


    public  void ChooseLetter( int i)
    {
        Assert.assertEquals(TitleAuthPage.getText(), "Autori");
        System.out.printf(AuthorsLetters.get(i).getText());
        AuthorsLetters.get(i).click();

    }


}
