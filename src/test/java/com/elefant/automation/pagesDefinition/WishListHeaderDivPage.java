package com.elefant.automation.pagesDefinition;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import static com.elefant.automation.utils.BaseTest.driver;
import static com.elefant.automation.utils.TestUtils.mySleeper;

public class WishListHeaderDivPage {
    @FindBy(how = How.XPATH, using = "//DIV[@class='container display-wishlist']")
    public static WebElement WishlistContainer;

    @FindBy(how = How.XPATH, using = "//DIV[@class='carousel-item']")
    public static WebElement ElementFromWishlist;

    @FindBy(how = How.XPATH, using = "(//A[@href='/wishlist?mode'][text()='Wishlist'][text()='Wishlist'])[2]")
    private static WebElement WhisListDetails;

    @FindBy(how = How.XPATH, using = "(//A[@href='javascript:void(0)'][text()='Sterge din wishlist'][text()='Sterge din wishlist'])")
    private static WebElement DeteleFromWishList;


    @FindBy(how = How.XPATH, using = "//DIV[@class='header-wishlist-icon']")
    public static WebElement WishListButtonFromHeader;

    @FindBy(how = How.XPATH, using = "//IMG[@src='http://static.elefant.ro/fe/bundles/elefantfront/images/elefant_logo.png?v=2']")
    public static WebElement Logo2;

    /**
     * Verifies if the wishlist contains elements
     * Empty the wishlist
     * Display a message when wishlist is empty
     */

    public void CheckAndEmptyWishList(){
        Assert.assertTrue(WishListButtonFromHeader.isDisplayed());
        mySleeper(1000);
        WishListButtonFromHeader.click();
        mySleeper(1000);
        Assert.assertTrue(WishlistContainer.isDisplayed());
        Assert.assertTrue(ElementFromWishlist.isDisplayed());
        WhisListDetails.click();
        try {


        do {
            WebElement element = (new WebDriverWait(driver, 1000))
                    .until(ExpectedConditions.elementToBeClickable(DeteleFromWishList));
            DeteleFromWishList.click();

            mySleeper(2000);
        }
        while (
                DeteleFromWishList.isDisplayed()
                );}
                catch (NoSuchElementException NO){
                    System.out.println("All wishlist was deleted");
                }
//        Logo2.click();
}}
