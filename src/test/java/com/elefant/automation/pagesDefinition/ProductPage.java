package com.elefant.automation.pagesDefinition;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

import static com.elefant.automation.utils.TestUtils.mySleeper;

public class ProductPage extends MainPage{
    public String SecondProductName;

    @FindBy(how = How.ID, using = "add-to-cart")
    public static WebElement AddToCartBtn;

    @FindBy(how = How.XPATH, using = "//DIV[@class='add-to-cart-holder']")
    private static WebElement UnavailableProduct;


    @FindBy(how = How.XPATH, using = "//A[@id='add-to-wishlist']")
    private static WebElement AddToWishlist;

    @FindBy(how = How.XPATH, using = "//DIV[@class='header-shopping-cart header-loaded']")
    public static WebElement BasketIndicator;

    @FindBy(how = How.XPATH, using = "html/body/div[4]/div/div[2]/div/div[5]/h1[1]")
    public static WebElement ProductSecondName;

    @FindBy(how = How.XPATH, using = "//LI[@class='wishlist-add'][text()='Wishlist']")
    private static WebElement PersonalWishlist;

    @FindBy(how = How.XPATH, using = "//DIV[@class='header-wishlist-icon']")
    private static WebElement HearderWishlist;


    /**
     * Verifies if add to cart button is displayed and push it
     * If add to cart is not displayed, product is not available message is displayed
     */


    public  void AddToCart(){
        mySleeper(2000);
//        System.out.println(ProductSecondName.getText());
//        SecondProductName=ProductSecondName.getText();
        try {
          Assert.assertTrue(AddToCartBtn.isDisplayed());
            Assert.assertEquals(AddToCartBtn.getText(),"ADAUGA IN COS" );
        AddToCartBtn.click();
       mySleeper(3000);
           Assert.assertEquals(BasketIndicator.getText(), "1");
        }
catch (AssertionError A){
            UnavailableProduct.isDisplayed();
            System.out.println("Product is not available");}
    }
//    NoSuchElementException B |
    /**
     * Verifies if the cart is empty
     */

    public void CartEmpty(){

        Assert.assertEquals(BasketIndicator.getText(), "0");
    }

    /**
     * Verifies if add to wishlist exists
     * Then clicks it
     */

    public void AddToWishlist(){
        Assert.assertTrue(AddToWishlist.isDisplayed());
        TakeAPic();
        AddToWishlist.click();
        mySleeper(3000);
        Assert.assertTrue(PersonalWishlist.isDisplayed());
        TakeAPic();
        PersonalWishlist.click();
    }



    /**
     * Clicks on the basket icon
     */

    public void ClickBasket(){
        mySleeper(2000);
        Assert.assertTrue(BasketIndicator.isDisplayed());
        BasketIndicator.click();
    }


}
