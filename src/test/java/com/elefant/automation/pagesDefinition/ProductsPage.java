package com.elefant.automation.pagesDefinition;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.util.List;

public class ProductsPage {

    @FindAll({@FindBy(how = How.XPATH, using = "(//DIV[@class='col-sm-12 elf-title'])")})
    public static List<WebElement> ItemsList;

    @FindAll({@FindBy(how = How.CLASS_NAME, using = "elf-price")})
    public static List<WebElement> PricesList;

    @FindBy(how = How.XPATH, using = "//A[@class='btn show-order-filters dropdown-toggle']")
    private static WebElement FilterList;

    @FindBy(how = How.XPATH, using = "//A[@href='http://www.elefant.ro/list/ceasuri?filtersex=Femei%7E%7E%7EUnisex&filterprice=500+-+999'][text()='\n" +
            "                                                            Popularitate                                                        ']")
    private static WebElement FilterPopularity;

    @FindBy(how = How.XPATH, using = "//A[@href='http://www.elefant.ro/list/ceasuri?filtersex=Femei%7E%7E%7EUnisex&sortsales=desc&filterprice=500+-+999'][text()='\n" +
            "                                                            Bestseller                                                        ']")
    private static WebElement FilterBestseller;

    @FindBy(how = How.XPATH, using = "//A[@href='http://www.elefant.ro/list/ceasuri?filtersex=Femei%7E%7E%7EUnisex&sortprice=desc&filterprice=500+-+999'][text()='\n" +
            "                                                            Pret descrescator                                                        ']")
    private static WebElement FilterDescPrice;

    @FindBy(how = How.XPATH, using = "//A[@href='http://www.elefant.ro/list/ceasuri?filtersex=Femei%7E%7E%7EUnisex&sortprice=asc&filterprice=500+-+999'][text()='\n" +
            "                                                            Pret crescator                                                        ']")
    private static WebElement FilterAscPrice;

    @FindBy(how = How.XPATH, using = "//A[@href='http://www.elefant.ro/list/ceasuri?filtersex=Femei%7E%7E%7EUnisex&sortdiscount=desc&filterprice=500+-+999'][text()='\n" +
            "                                                            Discount                                                        ']")
    private static WebElement FilterDiscount;

    @FindBy(how = How.XPATH, using = "//A[@href='http://www.elefant.ro/list/ceasuri?filtersex=Femei%7E%7E%7EUnisex&sortsitePublishDate=desc&filterprice=500+-+999'][text()='\n" +
            "                                                            Noutati                                                        ']")
    private static WebElement FilterNew;

    @FindBy(how = How.XPATH, using = "//A[@href='http://www.elefant.ro/list/ceasuri?filtersex=Femei%7E%7E%7EUnisex&sortratingCount=desc&filterprice=500+-+999'][text()='\n" +
            "                                                            Numar de voturi                                                        ']")
    private static WebElement FilterVotes;

    @FindAll({@FindBy(how = How.XPATH, using = "html/body/div[3]/div[1]/div[1]/div[2]/div[5]/div/div/div/div[2]/div/")})
    public static List<WebElement> PagingList;

    public void changePage(){

        for (WebElement e :PagingList ){
            System.out.println("Change page");
            e.getText();
         e.click();
        }
    }


    /**
     * Filters products identified by their position
     */

    public  void FilterProducts(int i){

        FilterList.click();
        switch(i){
            case 1:FilterPopularity.click();
            case 2 : FilterBestseller.click();
            case 3 : FilterDescPrice.click();
            case 4 : FilterAscPrice.click();
            case 5 : FilterDiscount.click();
            case 6 : FilterNew.click();
            case 7 : FilterVotes.click();
        }
    }

    public static void printList( List<WebElement> Li){
        for (int i = 0; i <Li.size(); i++) {
            System.out.println(Li.get(i).getText());
//                System.out.println(Li.get(i).getText().substring(0, Li.get(i).getText().indexOf(",")));
    }}


    public static void printListPrice( List<WebElement> Li){
        for (int i = 0; i <Li.size(); i++) {

                System.out.println(Li.get(i).getText().substring(0, Li.get(i).getText().indexOf(",")));
        }

}}