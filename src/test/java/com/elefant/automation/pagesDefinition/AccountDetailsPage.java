package com.elefant.automation.pagesDefinition;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import static com.elefant.automation.utils.BaseTest.TakeAPic;
import static com.elefant.automation.utils.TestUtils.generateRandNumber;
import static com.elefant.automation.utils.TestUtils.mySleeper;

public class AccountDetailsPage {


    @FindBy(how = How.XPATH, using = "//INPUT[@id='sex_1']")
    public static WebElement ChooseMiss;

    @FindBy(how = How.XPATH, using = "//INPUT[@id='sex_2']")
    public static WebElement ChooseMister;

    @FindBy(how = How.XPATH, using = "//INPUT[@id='firstname']")
    public static WebElement FirstName;

    @FindBy(how = How.XPATH, using = "//INPUT[@id='lastname']")
    public static WebElement LastName;

    @FindBy(how = How.XPATH, using = "//INPUT[@id='email']")
    public static WebElement Email;

    @FindBy(how = How.XPATH, using = "(//INPUT[@type='submit'])[1]")
    public static WebElement SubmitButon;

    @FindBy(how = How.XPATH, using = "(//SELECT[@class='form-control'])[1]")
    public static WebElement BirthDay;

    @FindBy(how = How.XPATH, using = "(//SELECT[@class='form-control'])[2]")
    private WebElement BirthMonth;

    @FindBy(how = How.XPATH, using = "(//SELECT[@class='form-control'])[3]")
    private WebElement Birthyear;


     @FindBy(how = How.XPATH, using = ".//*[@id='detalii-cont']/div[5]/div[1]/select")
    private WebElement Birthday;

    @FindBy(how = How.XPATH, using = ".//*[@id='detalii-cont']/input")
    private WebElement SaveDetailsButton;
    /**
     * Verify if miss is selected, selects mister, else select miss
     * Verify if Irina is the first name, changes it to Alina, else add first name Irina
     * Verify if email is ok, if not, change it
     * Also verifies the birthdate and change it
     * Change the birth date to a random one
     */
    public void VerifyAccountDetalis(){
        if (ChooseMiss.isSelected()==true){
            ChooseMister.click();
            TakeAPic();
        }
        else
        {ChooseMiss.click();
            TakeAPic();}
 try{
        Assert.assertEquals(FirstName.getText(), "Irina");
     TakeAPic();
        }
        catch (AssertionError A){
            FirstName.click();
            FirstName.clear();
            FirstName.sendKeys("Irina");
        }

        try{
            Assert.assertEquals(LastName.getText(), "Alina");
            TakeAPic();
        }
        catch (AssertionError A){
            LastName.click();
            LastName.clear();
            LastName.sendKeys("Alina");
            TakeAPic();
        }


        try{
            Assert.assertEquals(Email.getText(), "alina.irina.101@gmail.com");
            TakeAPic();}
        catch (AssertionError A){
            Email.click();
            Email.clear();
            Email.sendKeys("alina.irina.101@gmail.com");
            TakeAPic();
        }

        Assert.assertTrue(BirthDay.isDisplayed());
            Select MyBirthDay = new Select(Birthday);
            MyBirthDay.selectByIndex( generateRandNumber(1, 30));
            mySleeper(1000);
            Select MyBirthMonth = new Select(BirthMonth);
            MyBirthMonth.selectByIndex( generateRandNumber(1, 12));
        Select MyBirthYear= new Select(Birthyear);

        MyBirthYear.selectByValue(Integer.toString(generateRandNumber(1930, 2000)));
        TakeAPic();
        SaveDetailsButton.click();
        TakeAPic();
    }

}




