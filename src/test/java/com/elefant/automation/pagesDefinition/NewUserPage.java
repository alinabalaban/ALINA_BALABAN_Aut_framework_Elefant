package com.elefant.automation.pagesDefinition;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

import java.util.Random;

import static com.elefant.automation.utils.BaseTest.TakeAPic;
import static com.elefant.automation.utils.TestUtils.mySleeper;

public class NewUserPage {
    public String EmailAdress;
    Random rand = new Random();

    @FindBy(how = How.XPATH, using = ".//*[@id='sex_dna']")
    public static WebElement ChooseMiss;

    @FindBy(how = How.XPATH, using = ".//*[@id='sex_dl']")
    public static WebElement ChooseMister;

    @FindBy(how = How.XPATH, using = ".//*[@id='r_first_name']")
    public static WebElement FirstName;

    @FindBy(how = How.XPATH, using = ".//*[@id='r_last_name']")
    public static WebElement LastName;

    @FindBy(how = How.XPATH, using = ".//*[@id='r_email']")
    public static WebElement Email;

    @FindBy(how = How.XPATH, using = ".//*[@id='r_password']")
    public static WebElement Password;

    @FindBy(how = How.XPATH, using = ".//*[@id='r_c_password']")
    public static WebElement ConfPassword;

    @FindBy(how = How.XPATH, using = ".//*[@id='newsletter_subscribe']")
    public static WebElement News;


    @FindBy(how = How.XPATH, using = ".//*[@id='terms_ok']")
    public static WebElement Terms;

    @FindBy(how = How.XPATH, using = ".//*[@id='register_classic']")
    public static WebElement RegisterButton;

    /**
     * Creates a new account with a random email
     */

    public void CreateAccountWithDetails(){
        ChooseMiss.click();
        FirstName.sendKeys("Alina");
        LastName.sendKeys("Irina");
        int max = 200;
        int min = 0;
        int randomNum = rand.nextInt((max - min) + 1) + min;
//        System.out.println(randomNum);
        EmailAdress="Alina"+"Irina"+randomNum+"@yahoo.com";
        Email.sendKeys(EmailAdress);
        Password.sendKeys("333333");
        ConfPassword.sendKeys("333333");
        Assert.assertEquals(Password.getText(),ConfPassword.getText() );
        TakeAPic();
        News.click();
        mySleeper();
//        Assert.assertTrue(News.isSelected());
        Terms.click();
        mySleeper();
//        Assert.assertTrue(Terms.isSelected());
        Assert.assertTrue(RegisterButton.isDisplayed());
        TakeAPic();
        RegisterButton.click();
    }



}
