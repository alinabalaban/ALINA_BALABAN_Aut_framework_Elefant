package com.elefant.automation.pagesDefinition;

import com.elefant.automation.utils.BaseTest;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

import java.util.List;

import static com.elefant.automation.utils.TestUtils.generateRandNumber;
import static com.elefant.automation.utils.TestUtils.mySleeper;

public class MainPage extends BaseTest{
    public   String FirstProductName;


    @FindAll({@FindBy(how = How.CLASS_NAME, using = "item")})
    private static List<WebElement> ProductFastList;

    @FindAll({@FindBy(how = How.CLASS_NAME, using = "title")})
    private static List<WebElement> ProductTitles;

    @FindBy(how = How.XPATH, using = "//DIV[@class='suggest-error'][text()='Nu au fost gasite rezultate']")
    private static WebElement NoElementsReturnedOnSearch;


    @FindBy(how = How.XPATH, using = "//DIV[@class='hidden-xs header-account-display']")
    private static WebElement MyAccount;

    @FindBy(how = How.XPATH, using = "//A[@href='/autentificare'][text()='Intra in cont']")
    public static WebElement ChooseLogin;


    @FindBy(how = How.XPATH, using = "//SPAN[@id='user_name']")
    public static WebElement LoggedUser;

    @FindBy(how = How.XPATH, using = "//DIV[@class='hidden-xs header-account-display']")
    public static WebElement NotLogged;

    @FindBy(how = How.CLASS_NAME, using = "form-control")
    public static WebElement SearchProduct;

    @FindBy(how = How.ID, using = "searchsubmit")
    public static WebElement SearchMagnif;


    @FindAll({@FindBy(how = How.CLASS_NAME, using = "thumbnail")})
    private static List<WebElement> ProductsResultSet;


    @FindBy(how = How.XPATH, using = "//A[@id='logout-button']")
    public static WebElement LogOut;

    @FindBy(how = How.XPATH, using = "(//A[@href='/contul-meu/setari-cont'][text()='Setari cont'][text()='Setari cont'])[1]")
    public static WebElement AccountDetailsMenu;


    @FindBy(how = How.XPATH, using = "//A[@data-toggle='dropdown'][contains(text(),'Parfumuri')]")
    public static WebElement ParfumePicker;

    @FindBy(how = How.XPATH, using = "//A[@data-toggle='dropdown'][text()='Carti']")
    public static WebElement BookPicker;

    @FindBy(how = How.XPATH, using = "//A[@data-toggle='dropdown'][text()='Ceasuri']")
    public static WebElement WatchPicker;

    @FindBy(how = How.XPATH, using = "//A[@data-toggle='dropdown'][contains(text(),'Jucarii, copii')]")
    public static WebElement ToyPicker;

    @FindBy(how = How.XPATH, using = "//A[@data-toggle='dropdown'][text()='Electro']")
    public static WebElement ElectroPicker;

    @FindBy(how = How.XPATH, using = "//A[@data-toggle='dropdown'][contains(text(),'Casa')]")
    public static WebElement HousePicker;

    @FindBy(how = How.XPATH, using = "//A[@data-toggle='dropdown'][contains(text(),'Fashion')]")
    public static WebElement FashionPicker;

    @FindBy(how = How.XPATH, using = "//A[@data-toggle='dropdown'][text()='Sport']")
    public static WebElement SportPicker;

    @FindBy(how = How.XPATH, using = "(//A[@class='nav-title'][text()='Top branduri'][text()='Top branduri'])[1]")
    public static WebElement TopPerfume;

    @FindBy(how = How.XPATH, using = "//A[@href='/ebooks'][text()='Toate eBook-urile']")
    public static WebElement AllEbooks;

    @FindBy(how = How.XPATH, using = "//A[@href='/list/ceasuri?filtersex=Femei~~~Unisex'][text()='Ceasuri de dama']")
    public static WebElement WomanWatch;

    @FindBy(how = How.XPATH, using = "//A[@href='/list/jucarii?sortsales=desc'][text()='Jucarii']")
    public static WebElement AllToys;

    @FindBy(how = How.XPATH, using = "//A[@href='/list/it/telefoane-tablete-laptopuri-1'][text()='Telefoane - Tablete - Laptopuri']")
    public static WebElement Phones;


    @FindBy(how = How.XPATH, using = "//A[@href='/list/petshop'][text()='PetShop']")
    public static WebElement Petshop;

    @FindBy(how = How.XPATH, using = "//A[@href='/list/bijuterii'][text()='Bijuterii']")
    public static WebElement Bijuu;


     @FindBy(how = How.XPATH, using = "//A[@href='/list/sport/echipamente-accesorii-1/alergare'][contains(text(),'Alergare')]")
     public static WebElement Cycling;


    @FindBy(how = How.XPATH, using = "//A[@href='/list/ceasuri?filtersex=Femei~~~Unisex&sortsales=desc&filterprice=500+-+999'][text()='Ceasuri intre 500 lei si 999 lei']")
    public static WebElement MostExpensiveWatch;

    @FindBy(how = How.XPATH, using = "//DIV[@class='hidden-xs header-account-display']")
    public static WebElement MainButton;

    @FindBy(how = How.XPATH, using = "//A[@href='/despre-noi'][text()='Despre noi']")
    public static WebElement AboutUsButton;

    @FindBy(how = How.XPATH, using = "//A[@href='/carti/autori'][text()='Autori']")
    public static WebElement Authors;

    @FindBy(how = How.XPATH, using = "//A[@href='/edituri'][text()='Edituri']")
    public static WebElement Publishers;

    @FindBy(how = How.XPATH, using = "//A[@href='/oferte-speciale'][text()='Oferte speciale']")
    public static WebElement Offers;

    @FindBy(how = How.XPATH, using = "//A[@href='/contact'][text()='Contact']")
    public static WebElement Contact;
    @FindBy(how = How.XPATH, using = "//IMG[@src='//static.elefant.ro/fe/bundles/elefantfront/images/elefant_logo.png?v=2']")
    public static WebElement LogoMain;

    public static void print(){
        mySleeper(2000);
        ChooseLogin.getText();
    }

    /**
     * Choose an element from the right corner
     */

    public static void ChooseElementFromRightList(WebElement Element){
        MainButton.click();
        mySleeper(2000);
        Assert.assertTrue(LogoMain.isDisplayed());
        TakeAPic();
        Element.click();
    }

    /**
     * Verifies if the user is logged in
     * Verifies if the user loggedin is the right one
     */
    public static void VerifyUSerLogged(){

        Assert.assertEquals(LoggedUser.getText(),"alina");
        TakeAPic();
    }

    /**
     * Verifies if the user is logged out
     */

    public static void VerifyUSerLogOut(){
        mySleeper(1000);
        System.out.println(NotLogged.getText());
        Assert.assertEquals(NotLogged.getText(),"Contul meu");
        mySleeper(1000);
        TakeAPic();
    }

    /**
     * Sends a text to the search bar
     */

    public  void SearchProduct(String ProductName){
        mySleeper(1000);
        SearchProduct.click();
        SearchProduct.clear();
        mySleeper(1000);
        SearchProduct.sendKeys(ProductName);
        mySleeper(2000);
        TakeAPic();
    }

    /**
     * Perfome the actual search, clicks on magnify
     * Selects the product from a certain position
     * Saves the product name in variable FirstProductName
     */
    public void chooseElementFromList(int i){
        SearchMagnif.submit();
        ProductsResultSet.get(i).click();
        FirstProductName=ProductTitles.get(i).getText();
        TakeAPic();
    }

    /**
     * Clicks on a random element from the short search list
     * If the list has less then 4 products, the list will be shorted
     * If the list is empty, a message should be displayed
     */

    public  void SelectProduct(){
        try {

            mySleeper(1000);
            Assert.assertTrue(NoElementsReturnedOnSearch.isDisplayed());
            Assert.assertEquals(NoElementsReturnedOnSearch.getText(), "Nu au fost gasite rezultate");
            System.out.println("No elements found for searched string ");

            TakeAPic();

        }
        catch (NoSuchElementException E)
        {
            mySleeper(1000);
            if (ProductFastList.size() > 0) {
                if (ProductFastList.size() > 4) {
                    int i = generateRandNumber(0, 3);
                    FirstProductName = ProductTitles.get(i).getText();
                    ProductFastList.get(i).click();
                    TakeAPic();
                } else {
                    int i = generateRandNumber(0, ProductFastList.size() - 1);
                    FirstProductName = ProductTitles.get(i).getText();
                    ProductFastList.get(i).click();
                    TakeAPic();
                }
            }
       }
    }

    /**
     * Hover a element and validate that a certain element can be found in the list with a certain text
     */

 public static void ValidateDatePicker(WebElement ElHov, WebElement ToBefound, String Name){
     hover(ElHov);
     Assert.assertTrue(ToBefound.isDisplayed());
     Assert.assertEquals(ToBefound.getText(), Name);
 }

    /**
     * Hover a element and choose and click a sub element from the list
     */
 public static void ChooseSomething(WebElement ElHov, WebElement ElemChoose){
     hover(ElHov);
     Assert.assertTrue(ElemChoose.isDisplayed());
     ElemChoose.click();
 }

    /**
     * Choose one static page from the bottom of the page
     */
    public static void ChooseStaticPage(int i){
        switch(i){
            case 1:AboutUsButton.click();
            break;
            case 2:Authors.click();
            break;
            case 3:Publishers.click();
            break;
            case 4:Offers.click();
            break;
            case 5:Contact.click();
            break;
        }

    }
}
