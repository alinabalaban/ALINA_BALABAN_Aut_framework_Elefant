package com.elefant.automation.pagesDefinition;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

public class AboutUsPage {
    @FindBy(how = How.XPATH, using = "(//SPAN[text()='Cod Unic de Inregistrare: RO 26396066'])[1]")
    public static WebElement IdentificationCode;
    @FindBy(how = How.XPATH, using = "(//SPAN[text()='Registrul Comertului: J40/6415/2010'])[1]")
    public static WebElement CommerceCode;

    @FindBy(how = How.XPATH, using = "(//SPAN[text()='Adresă sediu: Bvd. Dimitrie Pompeiu, nr. 5-7, etaj 8, sector 2, București '])[1]")
    public static WebElement Address;

    @FindBy(how = How.XPATH, using = "(//SPAN[contains(text(),'Tel.:')])[1]")
    public static WebElement Telephone;





    public  void CheckDetailsAbout(){
        Assert.assertEquals(IdentificationCode.getText(),"Cod Unic de Inregistrare: RO 26396066" );
        Assert.assertEquals(CommerceCode.getText(),"Registrul Comertului: J40/6415/2010" );
        Assert.assertEquals(Address.getText(),"Adresă sediu: Bvd. Dimitrie Pompeiu, nr. 5-7, etaj 8, sector 2, București" );
        Assert.assertEquals(Telephone.getText(),"Tel.: \u202A\u202A 031.9301\u202C\u202C" );
    }
}
