package com.elefant.automation.utils;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.Reporter;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class TestUtils extends BaseTest {
    public static List<String> QueryResultsName=new ArrayList<String>();
    public static List<String> QueryResultsPrice= new ArrayList<String>();
    public static List<String> ItemsListName = new ArrayList<String>();
    public static List<String> PriceListItems = new ArrayList<String>();

    public static void mySleeper(int millis) {
        try {
            Thread.sleep(millis);
        }
        catch (Exception ex) {

        }
    }

    public static void pressBack(){
        driver.navigate().back();
    }

    public static void mySleeper() {

        mySleeper(500);
    }

    public static void getSizeList(List L) {
        System.out.println("List size is" + L.size());
        for(int i=0;i<L.size();i++){
            System.out.println(L.get(i));
        }}


    public static Statement stm;
    public static void DatabaseConnection(String username, String password, String database) {
        try {
            System.out.println("Din Utils"+Username+Password+ Database);
            /**
             * Connection to database
             */
            ExtentReports ex = new ExtentReports("C:\\Rap\\report.html", false);
            Reporter.log("BeforeLogin");
            ExtentTest t1 = ex.startTest("DB_test");
            Connection conn = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/"+database+ "?useSSL=false&serverTimezone=UTC", username, password);
             stm = conn.createStatement();

            /**
             * If somenthing went wrong, display trace
             */
        } catch (SQLException sexc) {
            sexc.printStackTrace();
        }

    }
    public static void CreateNewTable( String table){
        try {
        /**
         * Drop table from database
         */
            System.out.println("DROP TABLE IF EXISTS "+ table+";");
        stm.execute("DROP TABLE IF EXISTS "+ table+";");
            /**
             * Creates table in database
             */
            System.out.println("create table IF NOT EXISTS "  +table+ "( ID int NOT NULL AUTO_INCREMENT, Name varchar(255), Price varchar(255),  PRIMARY KEY (ID) );");
            stm.execute("create table IF NOT EXISTS "  +table+ "( ID int NOT NULL AUTO_INCREMENT, Name varchar(255), Price varchar(255),  PRIMARY KEY (ID) );");
            /**
             * Delete all values from table
             */
            stm.execute("delete from "+table);


    }
    catch (SQLException EX){
        EX.printStackTrace();
    }
    }

    public static void InsertInTablesFromLists(List<WebElement>  A, List<WebElement>  B, String table){
        try{            /**
         * Insert into table name and price of the products displayed
         */
            for (int i = 0; i < A.size(); i++) {
                String Element = A.get(i).getText();
                String Price = B.get(i).getText().substring(0, B.get(i).getText().indexOf(","));
                ItemsListName.add(A.get(i).getText());
                PriceListItems.add(B.get(i).getText().substring(0, B.get(i).getText().indexOf(",")));
//                System.out.println(ItemsListName.get(i));
//                System.out.println(PriceListItems.get(i));
                stm.execute("insert into "+ table+" (Name, price) values (\"" + Element + "\"," + "\"" + Price + "\")");
            }}
        catch (SQLException E){
            E.printStackTrace();
        }

    }
    /**
     * Generate a rand number between min and max
     */

        public static  int generateRandNumber(int mininm, int maxim){
            Random rand = new Random();
            int randomNum = rand.nextInt((maxim - mininm) + 1) + mininm;
            return randomNum;
        }

        public static void CreateResultSet(String column, String table,  List<String> QueryResults){
            /**
             * Create a list with elements  from Query
             */
            try {
                QueryResults= new ArrayList<String>();
            ResultSet res = stm.executeQuery("Select "+column+ " from " +table+";");
            while (res.next()) {
                String str = res.getString(column);
                QueryResults.add(str);
            }}
            catch (SQLException Sexc){
                System.out.println("Error In QueryResults ");
                Sexc.printStackTrace()
                ;}
        }


        public static void compareLists(){
            Collections.sort(QueryResultsName);
            Collections.sort(ItemsListName);
            Collections.sort(QueryResultsPrice);
            Collections.sort(PriceListItems);
            Assert.assertEquals(QueryResultsName, ItemsListName);
            Assert.assertEquals(QueryResultsPrice, PriceListItems);
        }


    public static void PrintValue(WebElement WE){
        System.out.println(WE.getText());
    }

}
