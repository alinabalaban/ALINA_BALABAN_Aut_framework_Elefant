package com.elefant.automation.utils;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.FluentWait;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class BaseTest {


    public static String Directory="C:\\Teme_alina\\PrintScreen\\";
    public static String  Username;
    public static String Password;
    public static String Database;
    public static ExtentReports extent;
    public static ExtentTest test;
    public static WebDriver driver;
    public DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss_SSS");
    static  Date date = new Date();
    final String filePath = "C:\\Teme_alina\\Reports\\TestReportElefant"+dateFormat.format(date)+".html";


    public static void hover(WebElement WEBelement) {
        Actions actions = new Actions(driver);
        actions = actions.moveToElement(WEBelement);
        Action action = actions.build();
        action.perform();
    }

    public static void pressBack() {
        driver.navigate().back();
    }

    public static void TakeAPic(){
        File screenshotFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        DateFormat dateFormat2 = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss_SSS");
        Date date2 = new Date();
        File finalFile = new File("C:\\Teme_alina\\PrintScreen\\Poza"+dateFormat2.format(date2)+".png");
        try {
            FileUtils.copyFile(screenshotFile, finalFile);
        }
        catch (IOException IO){
            System.out.println("You have an error");
        }

    }

    public static void deleteAllFilesFromFolderFolder(String outputFolderName) {
        File directory = new File(outputFolderName);
        final File[] files = directory.listFiles();
        for(File file: directory.listFiles()){
            if (!file.isDirectory())
                file.delete();}
    }

    public  static void declareUPD(String username, String password, String database){
        Username = username;
        Password =password;
        Database=database;
    }

    @BeforeTest
    public static void startDriver() {
        System.setProperty("webdriver.chrome.driver", "src\\main\\Drivers\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("http://elefant.ro/#__sYWxpbmFfaXJpbmFfYkB5YWhvby5jb20");
        FluentWait<WebDriver> fluentWait = new FluentWait<>(driver)
                .withTimeout(30, TimeUnit.SECONDS)
                .pollingEvery(200, TimeUnit.MILLISECONDS)
                .ignoring(NoSuchElementException.class);
        deleteAllFilesFromFolderFolder("C:\\Teme_alina\\PrintScreen\\");

    }

    @AfterMethod
    protected void afterMethod(ITestResult result) {
        if (result.getStatus() == ITestResult.FAILURE) {
            test.log(LogStatus.FAIL, result.getThrowable());
        } else if (result.getStatus() == ITestResult.SKIP) {
            test.log(LogStatus.SKIP, "Test skipped " + result.getThrowable());
        } else {
            test.log(LogStatus.PASS, "Test passed");
        }

        extent.endTest(test);
        extent.flush();
    }

    @BeforeSuite

    public void beforeSuite() {
        extent = ExtentManager.getReporter(filePath);
    }

    @AfterSuite
    protected void afterSuite() {
        extent.close();
    }

//    @AfterTest
//    public static void stopDriver() {
//        driver.quit();
//    }


}
