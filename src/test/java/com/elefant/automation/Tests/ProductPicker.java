package com.elefant.automation.Tests;

import com.elefant.automation.pagesDefinition.ElefantBlackFriday;
import com.elefant.automation.pagesDefinition.MainPage;
import com.elefant.automation.pagesDefinition.ProductsPage;
import com.elefant.automation.utils.BaseTest;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import static com.elefant.automation.pagesDefinition.MainPage.*;
import static com.elefant.automation.pagesDefinition.ProductsPage.ItemsList;
import static com.elefant.automation.pagesDefinition.ProductsPage.PricesList;
import static com.elefant.automation.utils.TestUtils.*;

public class ProductPicker extends BaseTest {
    @Parameters({"username", "password", "database"})
    @Test
    public void SearchProductsProductPicker(String username, String password, String database)
    {
        /**
         * Verifies every product picker
         * Choose something
         * Adds all elements into a database
         */
        test = extent.startTest("ProductPicker");
        ElefantBlackFriday BlackFriday = PageFactory.initElements(driver, ElefantBlackFriday.class);
        BlackFriday.BackToSite();
        MainPage FirstPageDisplayed = PageFactory.initElements(driver, MainPage.class);
        ValidateDatePicker(ParfumePicker, TopPerfume,"Top branduri" );
        TakeAPic();
        ValidateDatePicker(BookPicker, AllEbooks,"Toate eBook-urile" );
        TakeAPic();
        ValidateDatePicker(WatchPicker, WomanWatch,"Ceasuri de dama" );
        TakeAPic();
        ValidateDatePicker(ToyPicker, AllToys,"Jucarii" );
        TakeAPic();
        ValidateDatePicker(ElectroPicker, Phones,"Telefoane - Tablete - Laptopuri" );
        TakeAPic();
        ValidateDatePicker(HousePicker, Petshop,"PetShop" );
        TakeAPic();
        ValidateDatePicker(FashionPicker, Bijuu,"Bijuterii" );
        TakeAPic();
        ValidateDatePicker(SportPicker, Cycling,"Alergare&Ciclism" );
        TakeAPic();
        ChooseSomething(WatchPicker, MostExpensiveWatch);
        ProductsPage AllProducts = PageFactory.initElements(driver, ProductsPage.class);
        AllProducts.FilterProducts(7);
        declareUPD(username,password,database  );
        DatabaseConnection(Username, Password, Database);
        CreateNewTable("ElefantElements");
        InsertInTablesFromLists(ItemsList,PricesList,"ElefantElements");

    }}
