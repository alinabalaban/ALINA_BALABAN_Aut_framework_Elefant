package com.elefant.automation.Tests;

import com.elefant.automation.pagesDefinition.AccountDetailsPage;
import com.elefant.automation.pagesDefinition.ElefantBlackFriday;
import com.elefant.automation.utils.BaseTest;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import static com.elefant.automation.pagesDefinition.MainPage.AccountDetailsMenu;
import static com.elefant.automation.pagesDefinition.MainPage.ChooseElementFromRightList;

public class AccountDetails  extends BaseTest {
    @Test
    /**
     * Goes to Account details, verifies if Account details asre as expected and then change them
     */
    public void VerifyAccountDetails(){
        test = extent.startTest("AccountDetails");
        ElefantBlackFriday BlackFriday = PageFactory.initElements(driver, ElefantBlackFriday.class);
        BlackFriday.BackToSite();
        ChooseElementFromRightList(AccountDetailsMenu);
        AccountDetailsPage AuthorizationDetails = PageFactory.initElements(driver, AccountDetailsPage.class);
        AuthorizationDetails.VerifyAccountDetalis();

    }
}
