package com.elefant.automation.Tests;

import com.elefant.automation.pagesDefinition.AboutUsPage;
import com.elefant.automation.pagesDefinition.AuthorsPage;
import com.elefant.automation.pagesDefinition.ElefantBlackFriday;
import com.elefant.automation.pagesDefinition.MainPage;
import com.elefant.automation.utils.BaseTest;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import static com.elefant.automation.pagesDefinition.MainPage.ChooseStaticPage;

public class ValidateStaticPage extends BaseTest {
    @Test
    public static void ValidateStaticPageMeth(){
        /**
         * Validates some static pages
         */
        test = extent.startTest("ValidateStaticPageMeth");
        ElefantBlackFriday BlackFriday = PageFactory.initElements(driver, ElefantBlackFriday.class);
        BlackFriday.BackToSite();
        MainPage FirstPageDisplayed = PageFactory.initElements(driver, MainPage.class);
        ChooseStaticPage(1);
        AboutUsPage AboutPage = PageFactory.initElements(driver, AboutUsPage.class);
        AboutPage.CheckDetailsAbout();
        pressBack();
        ChooseStaticPage(2);
        AuthorsPage AuthPAge = PageFactory.initElements(driver, AuthorsPage.class);
        AuthPAge.ChooseLetter(2);
        AuthPAge.ChooseLetter(24);

//        mySleeper(1000);
//        pressBack();
//        pressBack();
//        ChooseStaticPage(5);
//        ContactPage ContactPageStatic = PageFactory.initElements(driver, ContactPage.class);
//        ContactPage.CheckContactPage();
}}
