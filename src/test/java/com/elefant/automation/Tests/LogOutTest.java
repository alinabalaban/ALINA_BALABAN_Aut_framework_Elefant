package com.elefant.automation.Tests;

import com.elefant.automation.pagesDefinition.MainPage;
import com.elefant.automation.utils.BaseTest;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import static com.elefant.automation.pagesDefinition.MainPage.*;
import static com.elefant.automation.utils.TestUtils.mySleeper;

public class LogOutTest extends BaseTest {
    @Test
    public void Logout(){
        /**
         * User logs out
         */
        test = extent.startTest("Log Out");
        mySleeper(2000);
        MainPage FirstPageDisplayed = PageFactory.initElements(driver, MainPage.class);
    ChooseElementFromRightList(LogOut);
    VerifyUSerLogOut();
}}
