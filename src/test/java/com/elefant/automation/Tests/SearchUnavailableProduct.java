package com.elefant.automation.Tests;

import com.elefant.automation.pagesDefinition.ElefantBlackFriday;
import com.elefant.automation.pagesDefinition.MainPage;
import com.elefant.automation.pagesDefinition.ProductPage;
import com.elefant.automation.utils.BaseTest;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

public class SearchUnavailableProduct extends BaseTest {
    @Test
    public static void SearchUnexistingProduct(){
        /**
         * Search products that does not exist and display a message
         */
        test = extent.startTest("SearchUnexistingProduct");
        ElefantBlackFriday BlackFriday = PageFactory.initElements(driver, ElefantBlackFriday.class);
        BlackFriday.BackToSite();
        MainPage FirstPageDisplayed = PageFactory.initElements(driver, MainPage.class);
        FirstPageDisplayed.SearchProduct("gzv");
        ProductPage ProductsPage = PageFactory.initElements(driver, ProductPage.class);
        ProductsPage.SelectProduct();
    }}
