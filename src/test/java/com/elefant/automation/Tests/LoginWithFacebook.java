package com.elefant.automation.Tests;

import com.elefant.automation.pagesDefinition.ElefantBlackFriday;
import com.elefant.automation.pagesDefinition.LoginFacebookPage;
import com.elefant.automation.pagesDefinition.LoginPage;
import com.elefant.automation.pagesDefinition.MainPage;
import com.elefant.automation.utils.BaseTest;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import static com.elefant.automation.pagesDefinition.MainPage.ChooseElementFromRightList;
import static com.elefant.automation.pagesDefinition.MainPage.ChooseLogin;

public class LoginWithFacebook extends BaseTest {


        @Test
        public void LoginFacebook(){
            /**
             * Login with a third party - Facebook, with a certain user and pass
             */
            test = extent.startTest("LoginFacebook");
            ElefantBlackFriday BlackFriday = PageFactory.initElements(driver, ElefantBlackFriday.class);
            BlackFriday.BackToSite();
            MainPage FirstPageDisplayed = PageFactory.initElements(driver, MainPage.class);
            ChooseElementFromRightList(ChooseLogin);
            LoginPage LoginPageDisplayed = PageFactory.initElements(driver, LoginPage.class);
            LoginPageDisplayed.loginMethod(1);
            LoginFacebookPage LoginFacebook = PageFactory.initElements(driver, LoginFacebookPage.class);
            LoginFacebook.AddDetailsFacebookLogin();

        }
}
