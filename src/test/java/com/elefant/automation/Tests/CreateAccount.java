package com.elefant.automation.Tests;

import com.elefant.automation.pagesDefinition.*;
import com.elefant.automation.utils.BaseTest;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import static com.elefant.automation.pagesDefinition.MainPage.ChooseElementFromRightList;
import static com.elefant.automation.pagesDefinition.MainPage.ChooseLogin;

public class CreateAccount extends BaseTest {
    @Test
    public void CreateAccountTest(){
        /**
         * Access login meniu
         * Creates a new account with random email address
         */
        test = extent.startTest("CreateAccount");
        ElefantBlackFriday BlackFriday = PageFactory.initElements(driver, ElefantBlackFriday.class);
        BlackFriday.BackToSite();
        MainPage FirstPageDisplayed = PageFactory.initElements(driver, MainPage.class);
        ChooseElementFromRightList(ChooseLogin);
        LoginPage LoginPageDisplayed = PageFactory.initElements(driver, LoginPage.class);
        LoginPageDisplayed.CreateAccount();
        NewUserPage CreateUserWithDetails = PageFactory.initElements(driver, NewUserPage.class);
        CreateUserWithDetails.CreateAccountWithDetails();

        RegistrationSuccessful RegistrationReady = PageFactory.initElements(driver, RegistrationSuccessful.class);
        RegistrationReady.readyAndBack();

}}
