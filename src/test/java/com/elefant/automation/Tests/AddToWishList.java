package com.elefant.automation.Tests;

import com.elefant.automation.pagesDefinition.ElefantBlackFriday;
import com.elefant.automation.pagesDefinition.MainPage;
import com.elefant.automation.pagesDefinition.ProductPage;
import com.elefant.automation.pagesDefinition.WishListHeaderDivPage;
import com.elefant.automation.utils.BaseTest;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

public class AddToWishList extends BaseTest {
    @Test
    public void AddToWishListMethod(){
        /**
         * Goes to search, search "periuta de dinti"
         * Add it to wishlist
         * Check the wishlist
         * Empty wishlist
         */
    test = extent.startTest("AddToWishList");
    ElefantBlackFriday BlackFriday = PageFactory.initElements(driver, ElefantBlackFriday.class);
            BlackFriday.BackToSite();
    MainPage FirstPageDisplayed = PageFactory.initElements(driver, MainPage.class);
            FirstPageDisplayed.SearchProduct("Periuta de dinti");
            FirstPageDisplayed.SelectProduct();
            ProductPage ProductsPage = PageFactory.initElements(driver, ProductPage.class);
            ProductsPage.AddToWishlist();
            WishListHeaderDivPage WishListPage = PageFactory.initElements(driver, WishListHeaderDivPage.class);
            WishListPage.CheckAndEmptyWishList();


}}
