package com.elefant.automation.Tests;

import com.elefant.automation.pagesDefinition.BasketPage;
import com.elefant.automation.pagesDefinition.ElefantBlackFriday;
import com.elefant.automation.pagesDefinition.MainPage;
import com.elefant.automation.pagesDefinition.ProductPage;
import com.elefant.automation.utils.BaseTest;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;


public class SearchProductAndAddToCart extends BaseTest {

        @Test
    public void SearchProductAndSelectTest()
        {
            /**
             * Search a product
             * Add it to cart
             * Check the Cart
             * Empty Cart
             */
            test = extent.startTest("SearchProductAndAddToCart");
    ElefantBlackFriday BlackFriday = PageFactory.initElements(driver, ElefantBlackFriday.class);
            BlackFriday.BackToSite();
            MainPage FirstPageDisplayed = PageFactory.initElements(driver, MainPage.class);
            FirstPageDisplayed.SearchProduct("Placa de par");
            FirstPageDisplayed.SelectProduct();
            ProductPage ProductsPage = PageFactory.initElements(driver, ProductPage.class);
            ProductsPage.AddToCart();
//            /**
//             * This used to work.But in a certain momement somenthing changed and I didn't manage to make it work again
//             */
////            Assert.assertEquals(FirstPageDisplayed.FirstProductName,ProductsPage.SecondProductName );
            ProductsPage.ClickBasket();
            BasketPage BasketPageDisplayed = PageFactory.initElements(driver, BasketPage.class);
            BasketPageDisplayed.getThirdName();
            Assert.assertEquals(FirstPageDisplayed.FirstProductName,BasketPageDisplayed.ThirdProductName );
            BasketPageDisplayed.removeFromBasket();
            BlackFriday.BackToSite();
}
}