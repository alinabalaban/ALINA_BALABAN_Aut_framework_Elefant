package com.elefant.automation.Tests;

import com.elefant.automation.pagesDefinition.ElefantBlackFriday;
import com.elefant.automation.pagesDefinition.LoginPage;
import com.elefant.automation.pagesDefinition.MainPage;
import com.elefant.automation.utils.BaseTest;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import static com.elefant.automation.pagesDefinition.MainPage.ChooseElementFromRightList;
import static com.elefant.automation.pagesDefinition.MainPage.ChooseLogin;


public class Login extends BaseTest {
    @Test
    public void Test1() {
        /**
         * Login with a certain user and pass
         */
        test = extent.startTest("Login");
        ElefantBlackFriday BlackFriday = PageFactory.initElements(driver, ElefantBlackFriday.class);
        TakeAPic();
        BlackFriday.BackToSite();
        TakeAPic();
        MainPage FirstPageDisplayed = PageFactory.initElements(driver, MainPage.class);
        ChooseElementFromRightList(ChooseLogin);
        TakeAPic();
        LoginPage LoginPageDisplayed = PageFactory.initElements(driver, LoginPage.class);
        LoginPageDisplayed.loginMethod();

    }
}
