package com.elefant.automation.Tests;

import com.elefant.automation.pagesDefinition.ElefantBlackFriday;
import com.elefant.automation.pagesDefinition.MainPage;
import com.elefant.automation.pagesDefinition.ProductPage;
import com.elefant.automation.pagesDefinition.ProductsPage;
import com.elefant.automation.utils.BaseTest;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;


public class SearchProductWithnoStock extends BaseTest {
    @Test
    public static void SearchNegative(){
        /**
         * Search a product with no stock
         * Verifies the list of elements
         * Display a message
         */
        test = extent.startTest("SearchProductWithnoStock");
    ElefantBlackFriday BlackFriday = PageFactory.initElements(driver, ElefantBlackFriday.class);
            BlackFriday.BackToSite();
            MainPage FirstPageDisplayed = PageFactory.initElements(driver, MainPage.class);
        FirstPageDisplayed.SearchProduct("MIHAI EMINESCU La steaua. Lecturi scolare 10,54");
        ProductsPage PageWithAllProducts = PageFactory.initElements(driver, ProductsPage.class);
        PageWithAllProducts.changePage();
        ProductPage ProductPage = PageFactory.initElements(driver, ProductPage.class);
        ProductPage.chooseElementFromList(0);
        ProductPage.AddToCart();
        ProductPage.CartEmpty();
}
}
